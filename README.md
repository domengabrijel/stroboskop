# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git init
git add .
git commit -m "Priprava potrebnih JavaScript knjižnic"
git push

Naloga 6.2.3:
https://bitbucket.org/domengabrijel/stroboskop/commits/670d54f4bf857c7fdad8aafd001875142a8cb4b3

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/domengabrijel/stroboskop/commits/fdaed5597bc120a1e14021dff0d2e325e4dce984

Naloga 6.3.2:
https://bitbucket.org/domengabrijel/stroboskop/commits/d74f204cd4630f9808d9992e1cc53213ca70a5ef

Naloga 6.3.3:
https://bitbucket.org/domengabrijel/stroboskop/commits/f9b616cb74acb56401ae22ba7f884292f6640bc6

Naloga 6.3.4:
https://bitbucket.org/domengabrijel/stroboskop/commits/2c1ed3afac1affcf5712f8b4e6c2ef5dbacec1fc

Naloga 6.3.5:

git checkout master
git merge izgled

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/domengabrijel/stroboskop/commits/2d1ebe1048339979c16036fa744623e4718c3344

Naloga 6.4.2:
https://bitbucket.org/domengabrijel/stroboskop/commits/f3e1d217c3f99f422cb6f730b22e37c40cb1a9ba

Naloga 6.4.3:
https://bitbucket.org/domengabrijel/stroboskop/commits/27a45ddb324511449eb27dfcf61b406d012627d0

Naloga 6.4.4:
https://bitbucket.org/domengabrijel/stroboskop/commits/32231a54c4ef084199f95dbd18c8a76856a1f66a